Example [Hugo](https://gohugo.io) website.

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project.
1. Install `git` and `go`.
1. [Install](https://gohugo.io/getting-started/installing/) Hugo.
1. Install the theme as a Hugo module:

   ```shell
   hugo mod get -u github.com/theNewDynamic/gohugo-theme-ananke
   ```

1. Preview your project:

   ```shell
   hugo server
   ```

1. Add content.
1. Optional. Generate the website:

   ```shell
   hugo
   ```

Read more at Hugo's [documentation](https://gohugo.io/getting-started/).

## Use a custom theme

Hugo supports a variety of themes.

Visit <https://themes.gohugo.io/> and pick the theme you want to use. In the
Pages example, we use <https://themes.gohugo.io/themes/gohugo-theme-ananke/>.

### Use a custom theme using a Hugo module

The example [`.gitlab-ci.yml`](.gitlab-ci.yml) uses Hugo modules to import the theme.

To use your own theme, the following steps will help you recreate the hugo modules
that include the information of your theme. You must perform them locally:

1. Edit `config.toml` and comment out the already existing theme:

   ```plaintext
   #theme = ["github.com/theNewDynamic/gohugo-theme-ananke"]
   ```

1. Remove `go.mod` and `go.sum`:

   ```shell
   rm -f go.mod go.sum
   ```

1. Recreate the theme module:

   ```shell
   hugo mod init gitlab.com/pages/hugo
   ```

1. Recreate the checksum:

   ```shell
   hugo mod get -u github.com/theNewDynamic/gohugo-theme-ananke
   ```

1. Edit `config.toml` and add the theme back:

   ```plaintext
   theme = ["github.com/theNewDynamic/gohugo-theme-ananke"]
   ```

1. Commit all the changes and push them to your repository.

## Troubleshooting

### CSS is missing!

That means two things:

- Either that you have wrongly set up the CSS URL in your templates.
- Or the [`baseURL`](https://gohugo.io/getting-started/configuration/#baseurl) in [`config.toml`](/config.toml) is not corretly set. For more information see issue https://gitlab.com/pages/hugo/-/issues/18.

### Minify the assets

If you want to minify the assets (JS, CSS, images, etc.), take a look at the [Hugo documentation](https://gohugo.io/getting-started/configuration/#configure-minify) and at this [merge request](https://gitlab.com/pages/hugo/-/merge_requests/79).
